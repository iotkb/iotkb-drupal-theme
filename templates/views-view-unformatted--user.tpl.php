<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)): ?>
<?php
	$terms = taxonomy_get_term_by_name($title);
	$term = current($terms);
	$tid = $term->tid;
?>
  <?php print '<section class="clearfix grouped-by user-media"><div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 item wrapme"><a href="media-by-category?tid=' . $tid . '"><h2>' . $title . '</h2></a><span class="user-media-header">My Interests</span></div>'; ?>
<?php endif; ?>
<?php foreach ($rows as $id => $row): ?>
  <div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .'"';  } ?>>
    <?php print $row; ?>
  </div>
<?php
    endforeach;
    echo '</section>';
?>
