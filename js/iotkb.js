(function ($, undefined) {
    "use strict";

    /**
     * Because older version of IE die when console is used and the console
     * window is not open
     */
    $.log = function (data) {
        if (window.console) {
            console.log(data);
        }
    };

    /**
     * Display debug fields if GET key debug exists
     */
    $.fn.showDebugFields = function () {
        var self = $(this),
            directives = {};

        // Make sure the query string exists
        if (location.search) {
            // split on each query string key/value pair
            if ($.inArray("&", location.search) !== -1) {
                $.each(location.search.split("&"), function (i, v) {
                        var str, splitTmp;

                    // Truncate leading question mark if it exists
                    if (v[0] === "?") {
                        str = v.slice(1);
                    } else {
                        str = v;
                    }

                    // Pull out each key, and if it exists, value
                    if ($.inArray("=", str) !== -1) {
                        splitTmp = str.split("=");
                        directives[splitTmp[0]] = splitTmp[1];
                    } else {
                        directives[str] = null;
                    }
                });
            } else {
                if (location.search[0] === "?") {
                    directives[location.search.slice(1)] = null;
                } else {
                    directives[location.search] = null;
                }
            }

            if (directives.debug !== undefined) {
                self.removeClass(self.selector.slice(1));
            }
        }

    };

    /**
     * Fix for the user view not displaying the rating widget correctly
     */
    $.fn.stars = function () {
        return $(this).each(function () {
            var self = $(this),
                val = parseFloat(self.html()) / 20,
                size, span;

            val = Math.round(val * 4) / 4;
            size = Math.max(0, (Math.min(5, val))) * 16;
            span = $("<span />").width(size);
            self.html(span).css("visibility", "visible");
        });
    };

    /**
     * Finds the navigation matching the current node and removes all but
     * the navigation link immediately before and after it
     */
    $.fn.setMediaNav = function () {
        var self = $(this),
            linksContainer = self.find(".media-nav-thumbs"),
            nodeClasses = $("body").attr("class").split(" "),
            targetNid, prevLink, nextLink;

        $.each(nodeClasses, function () {
            if (/^page-node-\d+$/.test(this)) {
                targetNid = this.replace(/^page-node-/, "");
            }
        });
        // if no prev link then make last link prev
        // if no next link then make first link prev
        prevLink = linksContainer.find(".nid-" + targetNid).prev();
        nextLink = linksContainer.find(".nid-" + targetNid).next();
        if (prevLink.length === 0) {
            linksContainer.find(".views-row").last().show().addClass("prev");
        } else {
            prevLink.show().addClass("prev");
        }
        if (nextLink.length === 0) {
            linksContainer.find(".views-row").first().show().addClass("next");
        } else {
            nextLink.show().addClass("next");
        }
        linksContainer.find(".nid-" + targetNid).remove();
    };

    /**
     * Move the save to my learning link to the view top content 
     */
    $.fn.setMyLearning = function () {
        var learning = this,
            //target = $("#block-views-node-eva-block-1 .field-value");
            target = $("#my-learning-toggle-wrapper");

        //target.after(learning);
        target.html(learning);
        learning.show();
    };

    $(document).ready(function () {
        $('.views-debug').showDebugFields();
        $('.perc-to-stars span').stars();
        $('.node-type-media').setMediaNav();
        $('.wrapme').wrapInner('<div class="wrapper clearfix"></div>').show();
        $('.flag-my-learning').setMyLearning();
		
        $('div.img-responsive img').addClass('img-responsive');
        $('div.img-responsive').removeClass('img-responsive');
		
		$('.node-type-media ul.links').appendTo('#links-wrapper').show();
 
 /* 
		$('.node-eva-related-media > .view-content').wrap(function(){
			return '<div class="col-md-12"><div class="carousel slide" id="carousel-related"><div class="view-content carousel-inner">' + $(this).html() + '</div><a class="left carousel-control" href="#carousel-related" data-slide="prev"><i class="fa fa-caret-left fa-xl"></i></a><a class="right carousel-control" href="#carousel-related" data-slide="next"><i class="fa fa-caret-right fa-xl"></i></a></div></div>';
		});
	
		// Related Items carousel
		$('#carousel-related').carousel({
			interval: 2000
		})
		$('.carousel .item').each(function(){
			var next = $(this).next();
			if (!next.length) {
				next = $(this).siblings(':first');
			}
			next.children(':first-child').clone().appendTo($(this));
		  
			for (var i=0;i<2;i++) {
				next=next.next();
					if (!next.length) {
						next = $(this).siblings(':first');
					}
			
				next.children(':first-child').clone().appendTo($(this));
		  	}
		});

*/
  	});
}(jQuery));
