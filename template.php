<?php

/**
 * @file
 * template.php
 */

function iotkb_preprocess_html(&$variables) {
  drupal_add_css('//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css', array('type' => 'external'));
}

function iotkb_menu_link(array $variables) {
 
/**
 * Implements theme_menu_link().
 *
 * This code adds an icon <I> tag for use with icon fonts when a menu item
 * contains a CSS class that starts with "icon-". You may add CSS classes to
 * your menu items through the Drupal admin UI with the menu_attributes contrib
 * module.
 *
 * Originally written by lacliniquemtl.
 * Refactored by jwilson3 > mroji28 > driesdelaey.
 * @see http://drupal.org/node/1689728
 */
  
$element = $variables['element'];
  $sub_menu = '';
  
// If there is a CSS class on the link that starts with "icon-", create
  // additional HTML markup for the icon, and move that specific classname there.
  // Exclusion List for settings eg http://fontawesome.io/examples/
  
$exclusion = array(
    'fa-lg','fa-2x','fa-3x','fa-4x','fa-5x',
    'fa-fw',
    'fa-ul', 'fa-li',
    'fa-border',
    'fa-spin',
    'fa-rotate-90', 'fa-rotate-180','fa-rotate-270','fa-flip-horizontal','fa-flip-vertical',
    'fa-stack', 'fa-stack-1x', 'fa-stack-2x',
    'fa-inverse'
  );
  if (isset(
$element['#original_link']['options']['attributes']['class'])) {
    foreach (
$element['#original_link']['options']['attributes']['class'] as $key => $class) {
      if (
substr($class, 0, 3) == 'fa-' && !in_array($class,$exclusion)) {
        
// We're injecting custom HTML into the link text, so if the original
        // link text was not set to allow HTML (the usual case for menu items),
        // we MUST do our own filtering of the original text with check_plain(),
        // then specify that the link text has HTML content.
        
if (!isset($element['#original_link']['options']['html']) || empty($element['#original_link']['options']['html'])) {
          $element['#title'] = check_plain($element['#title']);
          $element['#localized_options']['html'] = TRUE;
        }
        
// Add the default-FontAwesome-prefix so we don't need to add it manually in the menu attributes
        $class = 'fa ' . $class;
        
// Create additional HTML markup for the link's icon element and wrap
        // the link text in a SPAN element, to easily turn it on or off via CSS.
        $element['#title'] = '<i class="' . $class . '"></i> <span>' . $element['#title'] . '</span>';
        
// Finally, remove the icon class from link options, so it is not printed twice.
        unset($element['#localized_options']['attributes']['class'][$key]);
        
// kpr($element); // For debugging.
      
}
    }
  }
  // Save our modifications, and call core theme_menu_link().
  $variables['element'] = $element;
  return theme_menu_link($variables);
}